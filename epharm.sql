-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 29, 2019 at 07:10 PM
-- Server version: 5.1.32
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `epharm`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `empid` double NOT NULL,
  `ename` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `desg` varchar(50) NOT NULL,
  `phone` double NOT NULL,
  `emails` varchar(150) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`empid`, `ename`, `address`, `gender`, `desg`, `phone`, `emails`, `status`) VALUES
(1, 'vivek', 'sgccss', 'Male', 'software Engineer', 9447498692, 'sgccss@gmail.com', 'A'),
(2, 'ram', 'ranvilla', 'Male', 'Accounts', 1234567891, 'sg@gmail.com', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `scode` double NOT NULL,
  `pname` varchar(150) NOT NULL,
  `cgst` double NOT NULL,
  `sgst` double NOT NULL,
  `rate` double NOT NULL,
  `mrp` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`scode`, `pname`, `cgst`, `sgst`, `rate`, `mrp`) VALUES
(1, 'paracetal 600 mg', 6, 6, 1, 2),
(1, 'tidomet plus', 6, 6, 2, 4),
(2, 'tidoment', 12, 12, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `invno` double NOT NULL,
  `orderno` double NOT NULL,
  `invdate` date NOT NULL,
  `mname` varchar(150) NOT NULL,
  `qty` double NOT NULL,
  `rate` double NOT NULL,
  `tot` double NOT NULL,
  `cgst` double NOT NULL,
  `cgstamt` double NOT NULL,
  `sgst` double NOT NULL,
  `sgstamt` double NOT NULL,
  `pay` double NOT NULL,
  `mfdate` date NOT NULL,
  `expdate` date NOT NULL,
  `mrp` double NOT NULL,
  `status` varchar(15) NOT NULL,
  `stock` double NOT NULL,
  `rno` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`invno`, `orderno`, `invdate`, `mname`, `qty`, `rate`, `tot`, `cgst`, `cgstamt`, `sgst`, `sgstamt`, `pay`, `mfdate`, `expdate`, `mrp`, `status`, `stock`, `rno`) VALUES
(1, 1, '2019-10-28', 'paracetal 600 mg', 100, 1, 100, 6, 6, 6, 6, 112, '2019-10-29', '2020-10-31', 2, 'U', 100, 4),
(2, 2, '2019-10-29', 'tidomet plus', 100, 2, 200, 6, 12, 6, 12, 224, '0000-00-00', '2019-10-30', 4, 'I', 0, 0),
(3, 3, '2019-10-29', 'tidoment', 100, 1, 100, 12, 12, 12, 12, 124, '2019-10-30', '2020-10-31', 3, 'U', 100, 8);

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorder`
--

CREATE TABLE IF NOT EXISTS `purchaseorder` (
  `orderno` double NOT NULL,
  `odate` date NOT NULL,
  `rdate` date NOT NULL,
  `scode` double NOT NULL,
  `pname` varchar(150) NOT NULL,
  `qty` double NOT NULL,
  `status` varchar(15) NOT NULL,
  `empid` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchaseorder`
--

INSERT INTO `purchaseorder` (`orderno`, `odate`, `rdate`, `scode`, `pname`, `qty`, `status`, `empid`) VALUES
(1, '2019-10-28', '2019-10-31', 1, 'paracetal 600 mg', 100, 'I', 1),
(2, '2019-10-28', '2019-10-29', 1, 'tidomet plus', 100, 'I', 1),
(3, '2019-10-29', '2019-10-30', 2, 'tidoment', 100, 'I', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rack`
--

CREATE TABLE IF NOT EXISTS `rack` (
  `nofr` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rack`
--

INSERT INTO `rack` (`nofr`) VALUES
(30);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `scode` double NOT NULL,
  `rdate` date NOT NULL,
  `cname` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `cperson` varchar(150) NOT NULL,
  `phone` double NOT NULL,
  `emails` varchar(150) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`scode`, `rdate`, `cname`, `address`, `cperson`, `phone`, `emails`, `status`) VALUES
(1, '2019-10-28', 'Johnson labs', 'Kochi', 'suma', 99, 'abc@gmail.com', 'A'),
(2, '2019-10-29', 'abc las', 'ktm', 'sunil', 1234567890, 'sgccss@gmail.com', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `useraccount`
--

CREATE TABLE IF NOT EXISTS `useraccount` (
  `uname` varchar(150) NOT NULL,
  `pword` varchar(150) NOT NULL,
  `rights` varchar(15) NOT NULL,
  `ids` double NOT NULL,
  `photo` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `useraccount`
--

INSERT INTO `useraccount` (`uname`, `pword`, `rights`, `ids`, `photo`) VALUES
('jasna', 'jasna', 'A', 1, 'img/8.jpg'),
('vivek', 'vivek', 'E', 1, 'img/2.jpg'),
('sunil', 'sunil', 'S', 2, 'img/6.jpg'),
('ram', 'ram', 'E', 2, 'img/11.jpg');
